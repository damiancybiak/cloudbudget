# CloudBudget #

This project contains Sass and Jest from Npm for tests and some JS Front-end scripts without backend. 

### What do you need for launch that project? ###

* Nothing
* Optionally for Unit test
  * Npm
* Later I'll implement PHP script for real send data from Form, so if you see php file, you have to use localhost like Xampp, Lampp, Mampp or different local http server for test form 

### Compile Sass to Css ###

* I use native "File Watcher" from WebStorm
* If you want to use another bundler, like Gulp, Webpack, you have to install it from npm and configure
* If you do that, push your changes to repo 

### Project Structure ###
Project structure is very basic but Sass' files are created a little bit like components

* Each files is component
* Each component contains selectors only for that component
* Each selectors contains @media-queries only for that selector

#### Disclaimer ####
    The redundancy is only on the media, not on the selectors, so it is easy to find a place, which we want to edit


### Contact ###

* cybiakdamian@gmail.com


### Finally - a joke ###
Ulubione miejsce programistów na lotnisku? - Terminal xD <br>
Ba dum tss
