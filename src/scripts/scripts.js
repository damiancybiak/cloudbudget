"use strict";
const iconBurger = document.querySelector(".navigation__burger");
const showMobileMenu = document.querySelector(".navigation__list");
const iconBurgerChecker = () => {
    iconBurger.classList.toggle("active");
    showMobileMenu.classList.toggle("active");
}
iconBurger.addEventListener("click", iconBurgerChecker);

const closeMobileMenuAfterClick = () => {
    iconBurger.classList.remove("active");
    showMobileMenu.classList.remove("active");
}
if (!showMobileMenu.classList.contains('active')) {
    showMobileMenu.addEventListener("click", closeMobileMenuAfterClick)
}

const nav = document.querySelector('.navigation')
window.addEventListener("scroll", function () {
    let checkHeight = window.scrollY;
    let checkWidth = window.innerWidth;
    if (checkHeight > 100 && checkWidth > 992) {
        nav.classList.add("active");
    } else {
        nav.classList.remove("active")
    }
});

document.querySelectorAll('.navigation__list li a[href^="#"]')
    .forEach(trigger => {
        trigger.onclick = function (e) {
            e.preventDefault();
            let hash = this.getAttribute('href');
            let target = document.querySelector(hash);
            let headerOffset = 100;
            let elementPosition = target.offsetTop;
            let offsetPosition = elementPosition - headerOffset;

            window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
            });
        };
    });

const form = document.querySelector("#contactForm");
const inputs = form.querySelectorAll("[required]");
const formMessage = form.querySelector('.footer__select');

function removeFieldError(field) {
    const errorText = field.nextElementSibling;
    if (errorText !== null) {
        if (errorText.classList.contains("form-error-text")) {
            errorText.remove();
        }
    }
}
function createFieldError(field, text) {

    const div = document.createElement("div");
    div.classList.add("form-error-text");
    div.innerText = text;
    if (field.nextElementSibling === null) {
        field.parentElement.appendChild(div);
    } else {
        if (!field.nextElementSibling.classList.contains("form-error-text")) {
            field.parentElement.insertBefore(div, field.nextElementSibling);
        }
    }
}

function toggleErrorField(field, show) {
    const errorText = field.nextElementSibling;
    if (errorText !== null) {
        if (errorText.classList.contains("form-error-text")) {
            errorText.style.display = show ? "block" : "none";
            errorText.setAttribute('aria-hidden', show);
        }
    }
}

function markFieldAsError(field, show) {
    if (show) {
        field.classList.add("field-error");
    } else {
        field.classList.remove("field-error");
        toggleErrorField(field, false);
    }
}

const correctSendForm = () => {
    const correctMessage = document.createElement('span');
    correctMessage.classList.add('correct-message')
    correctMessage.innerText = 'Formularz został wysłany';
    formMessage.appendChild(correctMessage);

    inputs.forEach(input => input.value = '')
}

form.setAttribute("novalidate", true);

for (const el of inputs) {
    el.addEventListener("input", e => markFieldAsError(e.target, !e.target.checkValidity()));
}

form.addEventListener("submit", e => {
    e.preventDefault();
    let formErrors = false;

    for (const el of inputs) {
        removeFieldError(el);
        el.classList.remove("field-error");

        if (!el.checkValidity()) {
            createFieldError(el, el.dataset.errorText);
            el.classList.add("field-error");
            formErrors = true;
        }
    }

    if (!formErrors) {
        // e.target.submit();
        correctSendForm();
    } else {
        form.querySelector('.correct-message').innerText = '';
    }
});

